// SoftwareRenderer1001.cpp : Defines the entry point for the application.
//

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <inttypes.h>
#include <string>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int, HWND&);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

uint8_t* backbuffer;
BITMAPINFO bmpi;

HDC mainHDC;

void doFrame(HWND hwnd);

LARGE_INTEGER g_timer_start;
LARGE_INTEGER g_timer_end;

LARGE_INTEGER g_perf_freq;

void resetTime() {
    g_timer_start.QuadPart = 0;
    g_timer_end.QuadPart = 0;
}

void startTimer() {
    QueryPerformanceCounter(&g_timer_start);
}

int stopTimer() {
    QueryPerformanceCounter(&g_timer_end);
    LONGLONG diff_in_ticks =  g_timer_end.QuadPart - g_timer_start.QuadPart;
    float diff_in_seconds = (float) diff_in_ticks /  (float) g_perf_freq.QuadPart;
    float diff_in_micros = diff_in_seconds * 1000 * 1000;
    return diff_in_micros;
}

void log(std::string msg) {
    OutputDebugStringA(msg.c_str());
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{

    HWND wnd;

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow, wnd))
    {
        return FALSE;
    }

    QueryPerformanceFrequency(&g_perf_freq);


    MSG msg;
    bool shallRun = true;
    while (shallRun) {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE) != 0) {
            if (msg.message == WM_QUIT) {
                shallRun = false;
            }
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        doFrame(wnd);
    }

    return (int) msg.wParam;
}

void setPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b) {
    int pixel_start_address = (4 * 800 * y) + (x * 4);
    backbuffer[pixel_start_address] = b;
    backbuffer[pixel_start_address + 1] = g;
    backbuffer[pixel_start_address + 2] = r;
    backbuffer[pixel_start_address + 3] = 0;
}


void drawLine(int x1, int y1, int x2, int y2, uint8_t r, uint8_t g, uint8_t b) {
    int vx = x2 - x1;
    int vy = y2 - y1;

    float d = 1;

    if (vx != 0) {
        d = (float)vy / (float)vx;
    }
    
    int dirX = vx < 0 ? -1 : 1;
    int dirY = vy < 0 ? -1 : 1;
    int moved = 0;
    setPixel(x1, y1, r, g, b);
    int x = x1;
    float candidate_y = y1;
    if (abs(vx) > 0) {
        while (moved < abs(vx)) {
            x = x + dirX;
            candidate_y = candidate_y + (d * dirY);
            setPixel(x, candidate_y, r, g, b);
            moved++;
        }
    }
    else {
        while (moved < abs(vy)) {
            x = x;
            candidate_y = candidate_y + (d * dirY);
            setPixel(x, candidate_y, r, g, b);
            moved++;
        }
    }
    
}

int posX = 300;
int dir = 1;

void moveTriangle() {
    if (posX > 550 || posX < 15) {
        dir *= -1;
    }
    posX += 5 * dir;
}

void drawTriangle() {
    drawLine(posX + 100, 100, posX + 145, 120, 200, 200, 200);
    drawLine(posX + 100, 100, posX + 145, 100, 200, 200, 200);
    drawLine(posX + 145, 100, posX + 145, 120, 200, 200, 200);

}

void doFrame(HWND hwnd) {
   
    startTimer();
    for (int i = 0; i < 800 * 600 * 4; i += 4) {
        backbuffer[i] = 100;
        backbuffer[i + 1] = 100;
        backbuffer[i + 2] = 100;
        backbuffer[i + 3] = 0;
        
    }

    setPixel(400, 400, 10, 10, 255);
   
    moveTriangle();
    drawTriangle();

    
    
    HDC dc = GetDC(hwnd);
   
    StretchDIBits(dc, 0, 0, 800, 600, 0, 0, 800, 600, backbuffer, &bmpi, DIB_RGB_COLORS, SRCCOPY);
    ReleaseDC(hwnd, dc);

    int frame_time = stopTimer();
    log("FT: " + std::to_string(frame_time) + "\n");

    // todo(gru): improve on the main game loop
    if (frame_time < 33000) {
        Sleep(33 - (frame_time / 1000));
    }
}


//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow, HWND& wnd)
{

    WNDCLASSA wcex = {};
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = WndProc;
    wcex.hInstance = hInstance;
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.lpszClassName = "thewinclass";

    RegisterClassA(&wcex);

    hInst = hInstance; // Store instance handle in our global variable

    wnd = CreateWindowExA(0, wcex.lpszClassName, "King2021 0.0.1", WS_OVERLAPPEDWINDOW|WS_VISIBLE,
      300, 200, 800, 600, nullptr, nullptr, hInstance, nullptr);

   if (!wnd)
   {
      return FALSE;
   }

   ZeroMemory(&bmpi, sizeof(BITMAPINFO));
   bmpi.bmiHeader.biWidth = 800;
   bmpi.bmiHeader.biHeight = 600;
   bmpi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
   bmpi.bmiHeader.biPlanes = 1;
   bmpi.bmiHeader.biBitCount = 32;
   bmpi.bmiHeader.biCompression = BI_RGB;
   
   backbuffer = (uint8_t*)VirtualAlloc(0, 4 * 800 * 600, MEM_COMMIT, PAGE_READWRITE);

   ShowWindow(wnd, nCmdShow);
   UpdateWindow(wnd);

   return TRUE;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...

           /* for (int i = 0; i < 800 * 600 * 4; i += 4) {
                backbuffer[i] = 15;
                backbuffer[i + 1] = 10;
                backbuffer[i + 2] = 25;
                backbuffer[i + 3] = 0;
            }


            StretchDIBits(hdc, 0, 0, 800, 600, 0, 0, 800, 600, backbuffer, &bmpi, DIB_RGB_COLORS, SRCCOPY);
            */

            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

